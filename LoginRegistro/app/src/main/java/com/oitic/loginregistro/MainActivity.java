package com.oitic.loginregistro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView sesion;
    LinearLayout regist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        regist = (LinearLayout)findViewById(R.id.regist);
        sesion = (TextView)findViewById(R.id.start);

        sesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity_login_user(v);
            }
        });

        regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity_regitrer_user(v);
            }
        });

    }

    public void activity_login_user(View view) {

        Intent open = new Intent(MainActivity.this, LoginUser.class);
        startActivity(open);

    }

    public void activity_regitrer_user(View view) {
        Intent open2 = new Intent(MainActivity.this, registrerUser.class);
        startActivity(open2);
    }

}
